%global _empty_manifest_terminate_build 0
Name:           python-WSME
Version:        0.12.1
Release:        1
Summary:        Simplify the writing of REST APIs, and extend them with additional protocols.
License:        MIT
URL:            https://opendev.org/x/wsme
Source0:        https://files.pythonhosted.org/packages/3b/ae/fb85318a41e667508544bf0e20315469e9551d3af5fae3ac83217c1bb897/WSME-%{version}.tar.gz
BuildArch:      noarch
%description
Web Services Made Easy (WSME) simplify the writing of REST APIs, and extend
them with additional protocols.

%package -n python3-wsme
Summary:        Simplify the writing of REST APIs, and extend them with additional protocols.
Provides:       python-wsme
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-webob
BuildRequires:  python3-netaddr
BuildRequires:  python3-pytz
BuildRequires:  python3-simplegeneric
# General requires
Requires:       python3-webob
Requires:       python3-netaddr
Requires:       python3-pytz
Requires:       python3-simplegeneric
%description -n python3-wsme
Web Services Made Easy (WSME) simplify the writing of REST APIs, and extend
them with additional protocols.

%package help
Summary:        Simplify the writing of REST APIs, and extend them with additional protocols.
Provides:       python3-wsme-doc
%description help
Web Services Made Easy (WSME) simplify the writing of REST APIs, and extend
them with additional protocols.

%prep
%autosetup -n WSME-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-wsme -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Mar 05 2024 wangqiang <wangqiang1@kylinos.cn> - 0.12.1-1
- Update to 0.12.1

* Wed Jul 20 2022 renliang16 <renliang@uniontech.com> - 0.11.0-1
- Upgrade package python3-wsme to version 0.11.0

* Thu Aug 05 2021 liusheng <liusheng2048@gmail.com> - 0.10.1-1
- Upgrade to version 0.10.1

* Thu Jan 28 2021 zhangy <zhangy1317@foxmail.com>
- Add buildrequires

* Thu Dec 31 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
